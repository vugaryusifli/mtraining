'use strict';

$(function () {

    var down = "../../images/down-arrow.png";
    var up = "../../images/up-arrow.png";

    $.fn.clickToggle = function(func1, func2) {
        var funcs = [func1, func2];
        this.data('toggleclicked', 0);
        this.click(function() {
            var data = $(this).data();
            var tc = data.toggleclicked;
            $.proxy(funcs[tc], this)();
            data.toggleclicked = (tc + 1) % 2;
        });
        return this;
    };

    $('#down-up').clickToggle(function () {
        $(".wrapper-2").animate({
            scrollTop: $('.wrapper-2').prop("scrollHeight")
        }, 2000);
        document.getElementById('img').src = up;
    }, function () {
        $('.wrapper-2').animate({
            scrollTop: 0
        }, 2000);
        document.getElementById('img').src = down;
    });

});
