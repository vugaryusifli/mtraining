'use strict';

$(function () {

    var swiperV = new Swiper('.swiper-container-v', {
        direction: 'vertical',
        autoplay: {
            delay: 2000
        },
        loop: true,
        speed: 1000,
        slidesPerView: 2,
        centeredSlides: true,
        grabCursor: true,
        effect: 'coverflow',
        coverflowEffect: {
            rotate: 50,
            stretch: 0,
            depth: 100,
            modifier: 1,
            slideShadows: true
        }
    });

    $('.swiper-container-v').on('mouseenter', function(){
        swiperV.autoplay.stop();
    }).on('mouseleave', function() {
        swiperV.autoplay.start();
    });

    var swiperH = new Swiper('.swiper-container-h', {
        deceration: 'horizontal',
        autoplay: {
            delay: 1500
        },
        loop: true,
        speed: 1000,
        grabCursor: true,
        effect: 'cube',
        cubeEffect: {
            shadow: true,
            slideShadows: true,
            shadowOffset: 20,
            shadowScale: 0.94
        }
    });

    $('.swiper-container-h').on('mouseenter', function(){
        swiperH.autoplay.stop();
    }).on('mouseleave', function() {
        swiperH.autoplay.start();
    });

    lightbox.option({
        showImageNumberLabel: false,
        wrapAround: true,
        positionFromTop: 0,
        disableScrolling: true
    });

});

